#ifndef STATE_H
#define STATE_H

#include <iostream>

class State {
public:
	int hp;
	int hpMax;

	State(int hp);
	~State();
};

#endif