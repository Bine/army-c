#ifndef Rogue_ATTACK_H
#define Rogue_ATTACK_H

#include <iostream>
#include "../Ability.h"

class RogueAttack : public Ability {
public:
    RogueAttack(Unit* owner, int damage);
    ~RogueAttack();

    void action(Unit* target);
};

#endif