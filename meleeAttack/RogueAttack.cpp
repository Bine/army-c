#include "RogueAttack.h"

RogueAttack::RogueAttack(Unit* owner, int damage) : Ability(owner, damage, "Rogue Attack") {}
RogueAttack::~RogueAttack() {}

void RogueAttack::action(Unit* target) {
    target->takeDamage(damage);
}