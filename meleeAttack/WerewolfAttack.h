#ifndef WEREWOLF_ATTACK_H
#define WEREWOLF_ATTACK_H

#include <iostream>
#include "../Ability.h"

class WerewolfAttack : public Ability {
public:
    int b;
    WerewolfAttack(Unit* owner, int damage);
    ~WerewolfAttack();

    void action(Unit* target);
};

#endif