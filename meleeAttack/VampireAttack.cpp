#include "VampireAttack.h"

VampireAttack::VampireAttack(Unit* owner, int damage) : Ability(owner, damage, "Vampire Attack") {}
VampireAttack::~VampireAttack() {}

void VampireAttack::action(Unit* target) {
    target->takeDamage(damage);

    // this->owner->state->hp += damage / 3;
    owner->getState()->hp += damage / 3;
    if ( owner->getState()->hp > owner->getState()->hpMax ) {
         owner->getState()->hp = owner->getState()->hpMax;
    }

    target->isAlive();

    owner->takeDamage(target->getAbility()->getDamage() / 2);

        if ( target->getClass() == VAMPIRE ) {
            target->getState()->hp +=  target->getAbility()->getDamage() / 6;
        
        if ( target->getState()->hp > target->getState()->hpMax ) {
             target->getState()->hp = target->getState()->hpMax;
        }
    }

    if ( target->getClass() != WEREWOLF || target->getClass() != VAMPIRE ) {
        target->setAbility(target->getVampireAbility());
        target->setClass(VAMPIRE);
    }
}