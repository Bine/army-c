#include "WerewolfAttack.h"

WerewolfAttack::WerewolfAttack(Unit* owner, int damage) : Ability(owner, damage, "Werewolf Attack") {}
WerewolfAttack::~WerewolfAttack() {}

void WerewolfAttack::action(Unit* target) {
    target->takeDamage(damage);

    target->isAlive();

    owner->takeDamage(target->getAbility()->getDamage() / 2);

    if ( target->getClass() == VAMPIRE  ) {
            target->getState()->hp +=  target->getAbility()->getDamage() / 6;
        
        if ( target->getState()->hp > target->getState()->hpMax ) {
             target->getState()->hp = target->getState()->hpMax;
        }
    }

    if ( target->getClass() != VAMPIRE || target->getClass() != WEREWOLF ) {
        target->setClass(WEREWOLF);
    }
}