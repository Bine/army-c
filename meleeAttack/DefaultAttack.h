#ifndef DEFAULT_ATTACK_H
#define DEFAULT_ATTACK_H

#include <iostream>
#include "../Ability.h"

class DefaultAttack : public Ability {
public:
	DefaultAttack(Unit* owner, int damage);
	~DefaultAttack();

	void action(Unit* target);
};

#endif