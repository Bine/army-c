#include "DefaultAttack.h"

DefaultAttack::DefaultAttack(Unit* owner, int damage) : Ability(owner, damage, "Default Attack") {}
DefaultAttack::~DefaultAttack() {}

void DefaultAttack::action(Unit* target) {
    target->takeDamage(damage);

    target->isAlive();

    owner->takeDamage(target->getAbility()->getDamage() / 2);

    if ( target->getClass() == VAMPIRE ) {
            target->getState()->hp +=  target->getAbility()->getDamage() / 6;
        
        if ( target->getState()->hp > target->getState()->hpMax ) {
             target->getState()->hp = target->getState()->hpMax;
        }
    }
}