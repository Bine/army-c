#ifndef VAMPIRE_ATTACK_H
#define VAMPIRE_ATTACK_H

#include <iostream>
#include "../Ability.h"

class VampireAttack : public Ability {
public:
    VampireAttack(Unit* owner, int damage);
    ~VampireAttack();

    void action(Unit* target);
};

#endif