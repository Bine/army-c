#include "SacredFire.h"

SacredFire::SacredFire(int division) : Spell(30) {
    setDivisionSpell(division);
}
SacredFire::~SacredFire() {}
    
void SacredFire::action(Unit* target) {
    if ( target->getClass() == VAMPIRE || target->getClass() == NECROMANCER )  {
        target->takeMagicDamage(spellpower * 2);
    } else target->takeMagicDamage(spellpower);
}