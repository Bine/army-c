#include "DarknessArrow.h"

DarknessArrow::DarknessArrow() : Spell(30) {
}
DarknessArrow::~DarknessArrow() {}
    
void DarknessArrow::action(Unit* target) {
    target->takeMagicDamage(spellpower);
}
