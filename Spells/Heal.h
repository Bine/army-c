#ifndef HEAL_H
#define HEAL_H

#include "../Spell.h"

class Heal : public Spell {
public:
    Heal(int division);
    ~Heal();
    
    void action(Unit* target);
};

#endif