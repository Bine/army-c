#ifndef FIREBALL_H
#define FIREBALL_H

#include "../Spell.h"

class Fireball : public Spell {
public:
	Fireball(int division);
	~Fireball();
	
	void action(Unit* target);
};

#endif