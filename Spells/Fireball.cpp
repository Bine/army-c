#include "Fireball.h"

Fireball::Fireball(int division) : Spell(30) {
    setDivisionSpell(division);
}
Fireball::~Fireball() {}
	
void Fireball::action(Unit* target) {
    target->takeMagicDamage(spellpower);
}