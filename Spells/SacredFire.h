#ifndef SACREDFIRE_H
#define SACREDFIRE_H

#include "../Spell.h"

class SacredFire : public Spell {
private:
public:
    SacredFire(int division);
    ~SacredFire();
    
    void action(Unit* target);
};

#endif