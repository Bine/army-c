#include "Heal.h"

Heal::Heal(int divison) : Spell(30) {
    setDivisionSpell(divison);
}
Heal::~Heal() {}
    
void Heal::action(Unit* target) {
    target->takeHeal(spellpower);
}