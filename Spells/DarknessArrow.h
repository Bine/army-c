#ifndef DARKNESS_ARROW_H
#define DARKNESS_ARROW_H

#include "../Spell.h"

class DarknessArrow : public Spell {
public:
    DarknessArrow();
    ~DarknessArrow();
    
    void action(Unit* target);
};

#endif