#include "Spellcaster.h"

Spellcaster::Spellcaster(const std::string& name) 
	: Unit(name, new State(70), new DefaultAttack(this, 16)) {
		this->mana = 50;
		this->manaMax = mana;
	}
Spellcaster::~Spellcaster() {
	// std::map<std::string, Spell*>::const_iterator it = spellbook.begin();
	// for( ;  it != spellbook.end(); it++) {
		// delete;
	// }
}
	
void Spellcaster::cast(Unit* target, const std::string& spellname) {
	std::map<std::string, Spell*>::const_iterator pos = spellbook.find(spellname);

	if (pos == spellbook.end()) {
    	throw new NoSuchSpellException();
	} else {
    	pos->second->action(target);
    	if ( mana == 0 ) {
    	    throw new NoManaExeption();
    	}
	mana -= 5;
	}
}

int Spellcaster::getMana() {
	return mana;
}

int Spellcaster::getManaMax() {
	return manaMax;
}

std::ostream& operator<<(std::ostream& out, Spellcaster& u) {
	out << u.getName() << ": HP(" << u.getState()->hp;
	out << "/" << u.getState()->hpMax << ")";
	out << " MP(" << u.getMana() << "/" << u.getManaMax() << ")";

	return out;
}