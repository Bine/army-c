#ifndef SPELL_H
#define SPELL_H

#include <iostream>
#include "Unit.h"

class Spell {
protected:
	int spellpower;

public:
	Spell(int spellpower);
	~Spell();
	 virtual void action(Unit* target) = 0;
                void setDivisionSpell(int value);
};

#endif