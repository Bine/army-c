#include <iostream>

#include "Class/Soldier.h"
#include "Class/Wizard.h"
#include "Class/Rogue.h"
#include "Class/Berserker.h"
#include "Class/Vampire.h"
#include "Class/Werewolf.h"
#include "Class/Priest.h"
#include "Class/Warlock.h"
#include "Class/Necromancer.h"
#include "Class/Healer.h"

int main() {
	Soldier* s1 = new Soldier("Soldier_1");
	Wizard* w1 = new Wizard("Wizard_1");
	Rogue* r1 = new Rogue("Rogue_1");
	Healer* healer = new Healer("Healer");
	Vampire* v1 = new Vampire("Vampire_1");
	Priest* p1 = new Priest("Priest");
	Warlock* wl1 = new Warlock("Warlock");
	Werewolf* werewolf = new Werewolf("Werewolf");
	Necromancer* n1 = new Necromancer("Necromancer");

	std::cout << *w1<< std::endl;
	std::cout << *healer << std::endl;

	w1->cast(healer, "FIREBALL");
	w1->cast(healer, "FIREBALL");

	healer->cast(healer, "HEAL");
	healer->cast(healer, "HEAL");
	healer->cast(healer, "HEAL");

	w1->cast(healer, "FIREBALL");

	std::cout << "\n" << std::endl;


	std::cout << *w1 << std::endl;
	std::cout << *healer << std::endl;
	// std::cout << *v1 << std::endl;

	// werewolf->transform(WEREWOLF_FORM);

	// werewolf->getUnit()->attack(s1);

	// std::cout << "\n" << std::endl;

	// std::cout << *werewolf << std::endl;
	// std::cout << *s1 << std::endl;

	// w1->cast(v1, "FIREBALL");

	// std::cout << "\n" << std::endl;

	// std::cout << *v1 << std::endl;
	// std::cout << *w1 << std::endl;

	delete s1;
	delete w1;
	delete r1;
	// delete b1;
	delete v1;
	delete p1;
	delete n1;
	delete wl1;
	delete werewolf;
	delete healer;

	return 0;
}