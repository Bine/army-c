#include "Werewolf.h"

Werewolf::Werewolf(const std::string& name) : Unit(name, new State(100), new DefaultAttack(this, 30)) {
    unit_class = WEREWOLF;
}

Werewolf::~Werewolf() {}