#ifndef NECROMANCER_H
#define NECROMANCER_H

#include "../Spellcaster.h"
#include "../Spells/DarknessArrow.h"

class Necromancer : public Spellcaster {
protected:
public:
    Necromancer(const std::string& name);
    ~Necromancer();

    virtual void attack(Unit* enemy);
};

#endif