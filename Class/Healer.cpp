#include "Healer.h"

Healer::Healer(const std::string& name) : Spellcaster(name) {
    unit_class = HEALER;
    spellbook["FIREBALL"] = new Fireball(2);
    spellbook["HEAL"] = new Heal(1);
}
Healer::~Healer() {}