#include "Wizard.h"

Wizard::Wizard(const std::string& name) : Spellcaster(name) {
    unit_class = WIZARD;
    spellbook["FIREBALL"] = new Fireball(1);
    spellbook["HEAL"] = new Heal(2);
}
Wizard::~Wizard() {}