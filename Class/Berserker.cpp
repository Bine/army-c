#include "Berserker.h"

Berserker::Berserker(const std::string& name) : Unit(name, new State(100), new DefaultAttack(this, 40)) {
    unit_class = BERSERKER;
}
Berserker::~Berserker() {}  

void Berserker::takeMagicDamage(int damage) {}