#ifndef ROGUE_H
#define ROGUE_H

#include "../Unit.h"
#include "../meleeAttack/RogueAttack.h"

class Rogue : public Unit {
public:
    Rogue(const std::string& name);
    ~Rogue();
};

#endif