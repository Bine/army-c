#include "Priest.h"

Priest::Priest(const std::string& name) : Spellcaster(name) {
    unit_class = PRIEST;
    spellbook["SACREDFIRE"] = new SacredFire(2);
    spellbook["HEAL"] = new Heal(1);
}
Priest::~Priest() {}