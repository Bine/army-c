#include "Necromancer.h"

Necromancer::Necromancer(const std::string& name) : Spellcaster(name) {
    unit_class = NECROMANCER;
    spellbook["DARKNESSARROW"] = new DarknessArrow();
}
Necromancer::~Necromancer() {}

     void Necromancer::attack(Unit* enemy) {
        ability->action(enemy);
        enemy->addObserver(this);
     }