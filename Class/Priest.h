#ifndef PRIEST_H
#define PRIEST_H

#include "../Spellcaster.h"
#include "../Spells/SacredFire.h"
#include "../Spells/Heal.h"

class Priest : public Spellcaster {
public:
    Priest(const std::string& name);
    ~Priest();
};

#endif