#include "Vampire.h"

Vampire::Vampire(const std::string& name) : Unit(name, new State(100), new VampireAttack(this, 30)) {
    unit_class = VAMPIRE;
}
Vampire::~Vampire() {}