#ifndef HEALER_H
#define HEALER_H

#include "../Spellcaster.h"
#include "../Spells/Fireball.h"
#include "../Spells/Heal.h"

class Healer : public Spellcaster {
public:
    Healer(const std::string& name);
    ~Healer();
};

#endif