#include "Soldier.h"

Soldier::Soldier(const std::string& name) : Unit(name, new State(100), new DefaultAttack(this, 30)) {
    unit_class = SOLDIER;
}
Soldier::~Soldier() {}