#ifndef WEREWOLF_H
#define WEREWOLF_H

#include <iostream>
#include "../Unit.h"
#include "../meleeAttack/DefaultAttack.h"


class Werewolf : public Unit {
public:
    Werewolf(const std::string& name);
    ~Werewolf();

};

#endif