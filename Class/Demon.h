#ifndef DEMON_H
#define DEMON_H

#include <iostream>
#include "../Unit.h"
#include "../meleeAttack/DefaultAttack.h"

class Demon : public Unit {
public:
    Demon(const std::string& name);
    ~Demon();
};

#endif