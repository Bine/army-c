#include "Demon.h"

Demon::Demon(const std::string& name) : Unit(name, new State(120), new DefaultAttack(this, 35)) {}
Demon::~Demon() {}