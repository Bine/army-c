#ifndef SOLDIER_H
#define SOLDIER_H

#include <iostream>
#include "../Unit.h"
#include "../meleeAttack/DefaultAttack.h"

class Soldier : public Unit {
public:
	Soldier(const std::string& name);
	~Soldier();
};

#endif