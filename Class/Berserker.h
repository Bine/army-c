#ifndef BERSERKER_H
#define BERSERKER_H

#include "../Unit.h"
#include "../meleeAttack/DefaultAttack.h"

class Berserker : public Unit {
public:
    Berserker(const std::string& name);
    ~Berserker();

    void takeMagicDamage(int damage);

};

#endif