#ifndef WARLOCK_H
#define WARLOCK_H

#include "../Spellcaster.h"
#include "Demon.h"

class dontInitDemonExeption {};

class Warlock : public Spellcaster {
private:
    Unit* demon;
public:
    Warlock(const std::string& name);
    ~Warlock();

    void demonCall(const std::string& petName);
    Unit* getDemon();
};

#endif