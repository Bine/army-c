#include "Warlock.h"

Warlock::Warlock(const std::string& name) : Spellcaster(name) {
    unit_class = WARLOCK;
    this->demon = NULL;
}
Warlock::~Warlock() {}

void Warlock::demonCall(const std::string& petName) {
    demon = new Demon(petName);
}

Unit* Warlock::getDemon() {
    if ( demon == NULL ) {
        throw new dontInitDemonExeption();
    }
    return demon;
}
