#include "Rogue.h"

Rogue::Rogue(const std::string& name) : Unit(name, new State(100), new RogueAttack(this, 25)) {
    unit_class = ROGUE;
}
Rogue::~Rogue() {}

