#ifndef ABILITY_H
#define ABILITY_H

#include <iostream>
#include "Unit.h"

class Unit;

class Ability {
protected:
	int damage;
	Unit* owner;
	std::string name;

public:
	Ability(Unit* owner, int damage, const std::string& name);
	~Ability();

	virtual void action(Unit* target) = 0;
	const int getDamage() const;
};

#endif