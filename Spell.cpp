#include "Spell.h"

Spell::Spell(int spellpower) : spellpower(spellpower) {}
Spell::~Spell() {}

void Spell::setDivisionSpell(int division) {
    spellpower /= division;
}