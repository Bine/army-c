#include "Unit.h"

#include "meleeAttack/WerewolfAttack.h"
#include "meleeAttack/VampireAttack.h"

Unit::Unit(const std::string& name, State* state, Ability* ability) {
	this->name = name;
	this->ability = ability;
	this->state = state;
                this->altAbility = new WerewolfAttack(this,60);
                this->vampireAbility = new VampireAttack(this, 30);
                this->form = false;
}
Unit::~Unit() {
	delete state;
	delete ability;
}

State* Unit::getState() const {
	return state;
}
const std::string& Unit::getName() const {
	return name;
}

Ability* Unit::getAbility() const {
	return ability;
}

const int Unit::getClass() const {
	return unit_class;
	}

int Unit::setClass(int unit_class) {
    this->unit_class = unit_class;
}
	
Ability* Unit::setAbility(Ability* otherAbility) {
    this->ability = otherAbility;
}

 Ability* Unit::getVampireAbility(){
    return vampireAbility;
}

void Unit::takeDamage(int damage) {
	isAlive();
	this->state->hp -= damage;
	if ( this->state->hp <= 0 ) {
                    this->state->hp = 0;
                }
}

void Unit::takeMagicDamage(int damage) {
	takeDamage(damage);
}

void Unit::takeHeal(int heal) {
	isAlive();
	this->state->hp += heal;
	if ( this->state->hp > this->state->hpMax ) this->state->hp = this->state->hpMax;
}

void Unit::addObserver(Unit* unit) {
        observers.insert(unit);
    }

void Unit::die() {
        std::set<Unit*>::iterator it = observers.begin();
        for( ;  it != observers.end(); it++) {
            (*it)->takeHeal(30);
        }
        // delete this;
    }

void Unit::isAlive() {
	if ( this->state->hp == 0 ) {
                      die();
	       delete this;
	       throw new UnitIsDeadException();
	}
}

void Unit::attack(Unit* enemy) {
               isAlive();
	ability->action(enemy);
}

void Unit::transform() {
    isAlive();
    if (unit_class == WEREWOLF)  {
            // calculate percentage hp and damage

        if ( !form ) {
            state->hpMax *= 2;
            state->hp *= 2;
            form = true;
        } else {
            state->hpMax /= 2;
            state->hp /= 2;
            form = false;
        }

        Ability* tempAbility;
        tempAbility = ability;

        ability = altAbility;
        altAbility = tempAbility;
    } else {
        std::cout << "This spell is not available" << std::endl;
    }
}

std::ostream& operator<<(std::ostream& out, Unit& u) {
	out << u.getName() << ": HP(" << u.getState()->hp;
	out << "/" << u.getState()->hpMax << ")";

	return out;
}