#ifndef SPELLCASTER_H
#define SPELLCASTER_H

#include "Unit.h"
#include "Spell.h"
#include "meleeAttack/DefaultAttack.h"
#include <map>

class NoSuchSpellException {};
class NoManaExeption {};

class Spellcaster : public Unit {
protected:
                int mana;
                int manaMax;
	std::map<std::string, Spell*> spellbook;

public:
	Spellcaster(const std::string& name);
	~Spellcaster();
	
	void cast(Unit* target, const std::string& spellname);

                int getMana();
                int getManaMax();
};

std::ostream& operator<<(std::ostream& out, Spellcaster& u);


#endif