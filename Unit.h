#ifndef UNIT_H
#define UNIT_H

#include <iostream>
#include <set>
#include "State.h"
#include "Ability.h"

// необходимо предусмотреть Exeption при вызове удаленного обьекта, ошибка: "memory relics"!

typedef enum {
	SOLDIER,
	ROGUE,
	BERSERKER,
	VAMPIRE,
	WEREWOLF,
	WIZARD,
	HEALER,
	PRIEST,
	WARLOCK,
	NECROMANCER
} Classes;

class Ability;

class UnitIsDeadException {};

class Unit {
protected:
	int unit_class;
	std::string name;
	State* state;
	Ability* ability;
	std::set<Unit*> observers;
	Ability* vampireAbility;
	Ability* altAbility;
	bool form;

public:
	Unit(const std::string& name, State* state, Ability* ability);
	~Unit();

	void isAlive();

	
	virtual void attack(Unit* enemy);
	void takeDamage(int damage);
	virtual void takeMagicDamage(int damage);
	void takeHeal(int heal);
	void addObserver(Unit* unit);
	void die();
	void transform();

	Ability* getAbility() const;
	 State* getState() const;
	const std::string& getName() const;
	const int getClass() const;
	int setClass(int unit_class);
	Ability* setAbility(Ability* otherAbility);
	Ability* getVampireAbility();
};

std::ostream& operator<<(std::ostream& out, Unit& u);

#endif