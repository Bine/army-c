#include "Ability.h"

Ability::Ability(Unit* owner, int damage, const std::string& name) {
	this->owner = owner;
	this->damage = damage;
	this->name = name;
}
Ability::~Ability() {}

const int Ability::getDamage() const {
	return damage;
}